const plugin = require('../');
const eq = require('assert').deepEqual;

eq(
	plugin([{
		type: 'rule',
		selectors: ['body', 'div'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: 'rgb(28,29,233)'
		}, {
			type: 'property',
			name: 'background',
			value: 'red'
		}]
	}, {
		type: 'comment',
		text: 'ok'
	}, {
		type: 'rule',
		selectors: ['body', 'div'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: 'rgb(28,29,233)'
		}, {
			type: 'property',
			name: 'background',
			value: 'linear-gradient(to right, blue, red)'
		}]
	}]),

	[{
		type: 'rule',
		selectors: ['body', 'div'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: 'rgb(28,29,233)'
		}, {
			type: 'property',
			name: 'background',
			value: 'red'
		}, {
			type: 'property',
			name: 'background',
			value: 'linear-gradient(to right, blue, red)'
		}]
	}, {
		type: 'comment',
		text: 'ok'
	}]
);