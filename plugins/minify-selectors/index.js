/*
h1 + p, h4, h2, h3, h2{ color: red } -> h1 + p, h2, h3, h4{ color: red }
*/

function minifySelector(selectors) {
	const set = new Set(selectors); // unique
	return Array.from(set).sort();
}

module.exports = rules => {
	rules.forEach(rule => {
		if (rule.type==='rule') {
			rule.selectors = minifySelector(rule.selectors);
		}
	});
	return rules;
};