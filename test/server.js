const server = require('../server');
const fetch = require('node-fetch');
const assert = require('assert');

const TEST_PORT = 3001;
const httpServer = server.listen(TEST_PORT, '0.0.0.0');

const post = html => fetch(`http://localhost:${TEST_PORT}`, {
	method: 'POST',
	body: html
}).then(r => r.text());

// test server
module.exports = (async () => {

	const minified = await post(`
<html>
	<body>
		hello world
		<style>
			.foo {
				background: #ff5588
			}
		</style>

		<style>
			.foo {
				color: rebeccapurple;
			}
		</style>
	</body>
</html>
`);

	assert.equal(minified, `<html><head></head><body>
		hello world
		<style>
.foo {
  background: #f58;
  color: #639;
}
</style>

		
	

</body></html>`);

})()
.then(() => {
	httpServer.close();
}, err => {
	httpServer.close();
	console.error(err);
	throw err;
});
