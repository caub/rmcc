const {JSDOM} = require('jsdom');
const mensch = require('mensch');
const plugins = require('./plugins');
// not using jsdom style parser (CSSOM), as explained here: https://github.com/NV/CSSOM#dont-use-it-if

module.exports = minify;


/*
	receives an html page, opts (options for https://github.com/brettstimmerman/mensch#stringifyast-options)

	returns {original, minified} object containing original string and processed one
*/
function minify(htmlStr, opts) {
	const window = new JSDOM(htmlStr).window;
	const document = window.document;

	const styles = document.querySelectorAll('style');
	if (!styles.length) { // nothing to do
		return {original: htmlStr.toString(), minified: htmlStr.toString()};
	}

	// concat all styles together
	let styleStr = styles[0].textContent.trim();

	for (let i=1; i<styles.length; i++) {
		styleStr += styles[i].textContent.trim();
		styles[i].remove();
	}

	const ast = mensch.parse(styleStr, {comments: true});

	for (const name in plugins) {
		const plugin = plugins[name];
		ast.stylesheet.rules = plugin(ast.stylesheet.rules);
	}
	const minifiedCss = mensch.stringify(ast, Object.assign({comments: true, indentation: '  '}, opts));

	styles[0].textContent = '\n' + minifiedCss.replace(/\n+/g, '\n') + '\n'; // I don't know why, but it gives too many \n's

	return {
		original: htmlStr.toString(),
		minified: (document.doctype ? `<!DOCTYPE ${document.doctype.name}>` : '') + document.documentElement.outerHTML
	};
} 
