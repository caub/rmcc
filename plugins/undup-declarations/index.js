
/*
merge rules with same cssText (we could try hard to factor efficiently)
*/

module.exports = rules => {

	const declarationMap = new Map(); // decl -> rule map

	const newRules =  [];

	//iterate rules, when we see a match in declarationMap combine rules
	rules.forEach(rule => {
		if (rule.type !== 'rule') {
			return newRules.push(rule);
		}

		const declarationText = rule.declarations.filter(decl => decl.type==='property').join('\n'); // excluded comments and such
		const dRule = declarationMap.get(declarationText);
		if (!dRule) {
			declarationMap.set(declarationText, rule);
			return newRules.push(rule);
		}

		// merge selectors, we could add possible comments in rule not in dRule
		dRule.selectors = Array.from(new Set(dRule.selectors.concat(rule.selectors))).sort();

	});

	return newRules;
};
