const plugin = require('../');
const eq = require('assert').deepEqual;

eq(
	plugin([{
		type: 'rule',
		selectors: ['div'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: 'rgb(28,29,233)'
		}]
	}, {
		type: 'comment',
		text: 'ok'
	}, {
		type: 'rule',
		selectors: ['.test'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: 'rgb(28,29,233)'
		}]
	}]),

	[{
		type: 'rule',
		selectors: ['.test', 'div'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: 'rgb(28,29,233)'
		}]
	}, {
		type: 'comment',
		text: 'ok'
	}]
);