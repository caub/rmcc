## [Rebelmail code challenge](https://gist.github.com/datajohnny/17c68076d76a0a7679e841725f195746)

- minifier: module for minifying HTML
- plugins: folder used by minifier with various strategies to minify
- server: HTTP endpoint, dockerizable

### Requirements

- run redis-server
- valid email API tokens (not impl yet)