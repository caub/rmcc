const fs = require('fs');
const minify = require('../minifier');
const eq = require('assert').equal;

const inputs = fs.readdirSync(__dirname+'/fixtures/input/');

inputs.forEach(name => {
	const htmlStr = fs.readFileSync(__dirname+'/fixtures/input/'+name).toString();
	const expected = fs.readFileSync(__dirname+'/fixtures/output/'+name).toString();

	const result = minify(htmlStr);
	// console.log(result.minified.length, htmlStr.length);
	eq(result.minified, expected)
});