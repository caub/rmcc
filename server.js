const minify = require('./minifier');
const express = require('express');
const redis = require('redis'); // persist requests results
// todo mail API mailgun? rebel?

const client = redis.createClient();

const log = process.env.NODE_ENV === 'test' ? 
	() => {} : 
	result => {
		client.hmset('rmcc', 'date', Date.now(), 'ori', result.original, 'min', result.minified);
	};


const PORT = process.env.PORT || 3000;
const HOST = '0.0.0.0';

const app = express();

app.use((req, res, next) => { // CORS middleware, for testing more easily
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Credentials', 'true');
	if (req.method.toUpperCase()=='OPTIONS') {
		res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE,SEARCH,COPY');
		res.setHeader('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
		res.setHeader('Access-Control-Max-Age', '86400');
		return res.status(204).end();
	}
	next();
});

app.use(require('body-parser').text()); // for passing email, either use .json() or pass it in a header or querystring

app.post('/', (req, res) => {
	try {
		const result = minify(req.body);
		log(result);
		res.send(result.minified);
	} catch(e) {
		res.status(400).send(`Invalid html in body`);
		console.error(e);
	}
});

if (module.parent) {
	module.exports = app;

} else { // run now
	app.listen(PORT, HOST);
	console.log(`Running on http://${HOST}:${PORT}`);
}
