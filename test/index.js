const plugins = require('../plugins');

const dashed = s => s.replace(/[A-Z]/g, c=>'-'+c.toLowerCase());

(async () => {

	for (const name in plugins) {
		const test = require(`../plugins/${dashed(name)}/__test__`);
	}

	require('./full');

	await require('./server');

	console.log('all tests passed');

})()
.then(() => {
	process.exit(0);
}, () => {
	process.exit(1);
});