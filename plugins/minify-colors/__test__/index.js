const plugin = require('../');
const eq = require('assert').deepEqual;

eq(
	plugin([{
		type: 'rule',
		selectors: ['body'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: 'rgb(28, 29, 233)'
		}, {
			type: 'property',
			name: 'border',
			value: '1px dashed chartreuse'
		}, {
			type: 'property',
			name: 'font-size',
			value: '12px'
		}, {
			type: 'comment',
			text: 'foo'
		}]

	}]),

	[{
		type: 'rule',
		selectors: ['body'],
		declarations: [{
			type: 'property',
			name: 'color',
			value: '#1c1de9'
		}, {
			type: 'property',
			name: 'border',
			value: '1px dashed #7fff00'
		}, {
			type: 'property',
			name: 'font-size',
			value: '12px'
		}, {
			type: 'comment',
			text: 'foo'
		}]
	}]
);