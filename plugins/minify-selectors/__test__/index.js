const plugin = require('../');
const eq = require('assert').deepEqual;

eq(
	plugin([{
		type: 'rule',
		selectors: ['body', 'div', '.stuff', '.stuff', 'div.stuff']
	}, {
		type: 'comment',
		text: 'ok'
	}, {
		type: 'rule',
		selectors: ['h2', 'a', 'a', 'h2', 'h2']
	}]),

	[{
		type: 'rule',
		selectors: ['.stuff', 'body', 'div', 'div.stuff']
	}, {
		type: 'comment',
		text: 'ok'
	}, {
		type: 'rule',
		selectors: ['a', 'h2']
	}]
);