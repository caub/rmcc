
/*
merge together rules with same selectors
*/
module.exports = rules => {

	const selectorMap = new Map(); // selector -> rule map // could make it more pure with a groupBy

	const newRules =  [];

	//iterate rules, when we see a match in selectorMap combine rules
	rules.forEach(rule => {
		if (rule.type !== 'rule') {
			return newRules.push(rule);
		}

		const selectorText = rule.selectors.join(',');

		const sRule = selectorMap.get(selectorText);
		if (!sRule) {
			selectorMap.set(selectorText, rule);
			return newRules.push(rule);
		}

		// merge rules without deplicate properties
		const map = new Map(sRule.declarations.map(d => [d.name ? d.name+':'+d.value : d.text, d]));
		rule.declarations.forEach(d => {
			if (!map.has(d.name+':'+d.value)) {
				sRule.declarations.push(d);
			}
		});
		// declaration with same name but different value can't always be overwritten https://github.com/NV/CSSOM#dont-use-it-if, it could be merged with browser-specific knowledge, not impl here
	});


	return newRules;
};