
exports.minifySelectors = require('./minify-selectors');

exports.undupSelectors = require('./undup-selectors'); // must be after minify-selectors
exports.undupDeclarations = require('./undup-declarations');

exports.minifyColors = require('./minify-colors');