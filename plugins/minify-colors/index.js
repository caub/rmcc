const color = require('color');
const keywords = require('./keywords.json'); // named colors https://drafts.csswg.org/css-color/#named-colors
const nameHexMap = new Map(keywords);
const hexNameMap = new Map(keywords.map(([c, h]) => [h, c]));

const toLongHex = s => s.length === 4 ? '#'+s[1]+s[1]+s[2]+s[2]+s[3]+s[3] : s;
const toShortHex = s => s[1]===s[2] && s[3]===s[4] && s[5]===s[6] ? '#'+s[1]+s[3]+s[5] : s;

const cssNamesColorRe = /^(border|background|color$|outline)/; // todo, it's surely incomplete

// code inspired from https://github.com/ben-eb/cssnano/blob/master/packages/postcss-colormin/src/colours.js

function minifyColor(col) {
	try {
		const parsed = color(col.toLowerCase());
		const alpha  = parsed.alpha();
		if (alpha === 1) {
			const longHex = toLongHex(parsed.hex().toLowerCase());
			const shortHex = toShortHex(longHex);
			const name = hexNameMap.get(longHex);
			return name && name.length <= shortHex.length ? name : shortHex;
		}

		if (!alpha) return 'transparent';

		const rgba = parsed.rgb().string();
		const hsla = parsed.hsl().round().string();
		return hsla.length < rgba.length ? hsla : rgba;

	} catch(e) {
		// console.error(e);
		return col;
	}
}

module.exports = rules => {
	rules.forEach(({type, declarations}) => {
		if (type==='rule') {
			declarations.forEach(decl => {
				if (decl.type==='property' && cssNamesColorRe.test(decl.name)) {
					const vals = decl.value.match(/[#\w]+(?:\([^)]+\))?/g); // get all values
					decl.value = vals.map(minifyColor).join(' ');
				}
			});
		}
	});
	return rules;
};
